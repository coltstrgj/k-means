# K-Means Modified

This project will attempt to build a modified k-means clustering algorithm. 
The modification comes in that instead of updating the k cluster centers at the end of every iteration, we update them as the points move between them. 
I don't know how well it will go, or how much worse/better (Probably worse) it will be compared to the usual implementation of Lloyds algorithm.
After I try this, I will likely go into some other optimizations that have been proven, or that I want to try just for fun.  
